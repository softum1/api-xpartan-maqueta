import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "3.1.5"
	id("io.spring.dependency-management") version "1.1.3"
	jacoco
	kotlin("jvm") version "1.8.22"
	kotlin("plugin.spring") version "1.8.22"
	kotlin("plugin.jpa") version "1.8.22"
	id("io.gitlab.arturbosch.detekt") version "1.20.0"
	id("org.sonarqube") version "4.4.1.3373"
	kotlin("kapt") version "1.8.22"
}

group = "com"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
	mavenCentral()
}

extra["springCloudVersion"] = "2022.0.4"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation ("org.springframework.boot:spring-boot-starter-logging")
	implementation("jakarta.validation:jakarta.validation-api:3.0.2")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.flywaydb:flyway-core")
	implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
	implementation("org.springframework.kafka:spring-kafka")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("org.postgresql:postgresql")
	implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")
	implementation("org.mapstruct:mapstruct:1.5.5.Final")
	kapt("org.mapstruct:mapstruct-processor:1.5.5.Final")
	implementation("io.rest-assured:json-path:5.3.0")
	testImplementation("com.github.tomakehurst:wiremock:3.0.1")
	testImplementation("io.rest-assured:rest-assured:5.3.0")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.kafka:spring-kafka-test")

}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.bootBuildImage {
	builder.set("paketobuildpacks/builder-jammy-base:latest")
}

tasks.test {
	finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
	doFirst {
		delete(fileTree("$buildDir/classes/java").matching {
			include("**/*.class")
		})
	}
	reports {
		xml.required = true
	}
	dependsOn(tasks.test) // tests are required to run before generating the report
}

sonarqube {
	properties {
		property("sonar.host.url", "http://localhost:9000/")
		property("sonar.token", "sqp_05fda67565bfd17ce891ab78ad190bb72c39deb5")
		property("sonar.projectKey", "ApiXpartan")
		property("sonar.exclusions", "**/build.gradle.kts")
	}
}

tasks.named("sonar") {
	dependsOn("jacocoTestReport")
}

