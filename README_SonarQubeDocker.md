---
tags: 
Titulo: sonarqube en docker
Links:
  - https://github.com/SonarSource/docker-sonarqube/blob/a1a383e82d4b8b273c8b9237714fc9c700b71dc7/9/community/Dockerfile
Tema: SonarQube
Fecha: 2023-10-25
---
# ✍️ Notas
1. Descargar repositorio
```Bash
git clone https://github.com/SonarSource/docker-sonarqube.git
```
2. Ingresamos a la direccion `/9/community` las otras son de paga
```Bash
cd docker-sonarqube-a1a383e82d4b8b273c8b9237714fc9c700b71dc7/9/community/   
```
5. Una vez parados en la carpeta hay dos archivos `Dockerfile` y `entrypoint.sh`

Verificamos

![abc|700](files/sonar_images.png)

6. Hacemos correr `DockerFile` para que descargue y construya la imagen docker de sonarqube. Las configuraciones extras estan en `entrypoint.sh` que sera llamada dentro de `DockerFile`.
8. Hacemos correr el `DockerFile` con el comando
```bash
docker build -t sonarqube:v1 .
```
7. Comprobamos que la imagen se descargo
```
docker images
```
Verificamos

![abc|700](files/sonar_image1.png)

8. Hacemos correr nuestra imagen
```bash
docker run sonarqube:v1
```
verificamos

![abc|500](files/sonar_images2.png)

9. Ingresamos al Link en el navegador
```
http://localhost:9000
```
 si esta no funciona ingrese ha esta otra
```
http://172.17.0.2:9000/sessions/new?return_to=%2Fadmin%2Fsettings
```
 verificamos
![abc|700](files/sonar_images3.png)
por defecto el `usuario y password` es `admin`
# 🔗 Referencias
- [sonarqube_dockerHub_doc](https://hub.docker.com/_/sonarqube)