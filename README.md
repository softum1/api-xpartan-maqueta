# SPRING BOOT 

# JAVA 17
# Gradle
# Kotlin
# Database Postgres
- First make sure to create the database, with the following command:
```Bash
create database xpartan;
```
# download dependencies
- gradle dependencies

# run project
```Bash
gradle bootRun
```
or
```Bash
./gradlew bootRun
```