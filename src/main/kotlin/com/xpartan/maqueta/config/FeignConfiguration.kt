package com.xpartan.maqueta.config

import feign.Request
import org.springframework.cloud.openfeign.FeignClientsConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Suppress("DEPRECATION")
@Configuration
class FeignConfiguration : FeignClientsConfiguration() {

    companion object {
        const val CONNECT_TIMEOUT = 10000
        const val READ_TIMEOUT = 10000
    }
    @Bean
    fun requestOptions(): Request.Options {
        val connectTimeoutMillis = CONNECT_TIMEOUT // 10 segundos de tiempo de conexión
        val readTimeoutMillis = READ_TIMEOUT    // 10 segundos de tiempo de lectura
        return Request.Options(connectTimeoutMillis, readTimeoutMillis)
    }
}
