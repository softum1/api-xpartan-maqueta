package com.xpartan.maqueta.service.impl

import com.xpartan.maqueta.exceptions.CustomValidationException
import com.xpartan.maqueta.mapper.ExampleMapper
import com.xpartan.maqueta.model.ExampleDto
import com.xpartan.maqueta.repository.ExampleRepository
import com.xpartan.maqueta.service.IExampleService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class ExampleServiceImpl: IExampleService {

    @Autowired
    private lateinit var exampleRepository: ExampleRepository

    @Autowired
    private lateinit var exampleMapper: ExampleMapper

    companion object {
        const val ERROR = "Error desconocido"
        val logger: Logger = LoggerFactory.getLogger(ExampleServiceImpl::class.java)
    }

        override fun getHelloWorld(): String {
            logger.debug("Service to get Hello World")
            return "Hello World!!!"
        }

    override fun getAllExample(pageable: Pageable): Page<ExampleDto> {
        logger.debug("Service to get all example")
        return exampleRepository.findAll(pageable)
            .map { exampleMapper.toDto(it) }
    }

    override fun getExampleById(id: Long): ExampleDto? {
        logger.debug("Service to get example by id")
        try {
            val existExample = exampleRepository.findById(id)
                .orElseThrow { NoSuchElementException("No se encontró la entidad con ID: $id") }
            return exampleMapper.toDto(existExample)
        } catch (ex: NoSuchElementException) {
            logger.error("An error occurred!!! ${ex.message}")
            val errorMessage = ex.message ?: ERROR
            throw CustomValidationException(errorMessage)
        }
    }
    override fun saveExample(exampleDto: ExampleDto): ExampleDto {
            logger.debug("Service to save example : {}", exampleDto)
            val exampleEntity = exampleMapper.toEntity(exampleDto)
            return exampleMapper.toDto(this.exampleRepository.save(exampleEntity))
    }

    override fun updateExample(exampleDto: ExampleDto, id: Long): ExampleDto? {
        logger.debug("Service to update example : {}", exampleDto)
        try {
            val existExample = exampleRepository.findById(id)
                .orElseThrow { NoSuchElementException("No se encontró la entidad con ID: $id") }
            existExample.title = exampleDto.title
            val updateExample = exampleRepository.save(existExample)
            return exampleMapper.toDto(updateExample)
        } catch (ex: NoSuchElementException) {
            logger.error("An error occurred!!! ${ex.message}")
            val errorMessage = ex.message ?: ERROR
            throw CustomValidationException(errorMessage)
        }
    }

    override fun deleteExample(id: Long) {
        logger.debug("Service to delete example")
        try {
            exampleRepository.findById(id)
                .orElseThrow { NoSuchElementException("No se encontró la entidad con ID: $id") }
            exampleRepository.deleteById(id)
        } catch (ex: NoSuchElementException) {
            logger.error("An error occurred!!! ${ex.message}")
            val errorMessage = ex.message ?: ERROR
            throw CustomValidationException(errorMessage)
        }
    }
}
