package com.xpartan.maqueta.service

import com.xpartan.maqueta.model.ExampleDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface IExampleService {
    fun getHelloWorld() : String

    fun getExampleById(id: Long) : ExampleDto?

    fun getAllExample(pageable: Pageable) : Page<ExampleDto>

    fun saveExample(exampleDto: ExampleDto) : ExampleDto

    fun updateExample(exampleDto: ExampleDto, id: Long) : ExampleDto?

    fun deleteExample(id: Long) : Unit?

}
