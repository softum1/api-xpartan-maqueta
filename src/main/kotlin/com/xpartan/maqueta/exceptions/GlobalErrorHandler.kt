package com.xpartan.maqueta.exceptions

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.stream.Collectors

@Suppress("UNCHECKED_CAST")
@RestControllerAdvice
class GlobalErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleValidationErrors(ex: MethodArgumentNotValidException): ResponseEntity<Map<String, List<String>>> {
        val errors = ex.bindingResult.fieldErrors
            .stream().map(FieldError::getDefaultMessage).collect(Collectors.toList())
        return ResponseEntity(getErrorsMap(errors as List<String>), HttpHeaders(), HttpStatus.BAD_REQUEST)
    }

    private fun getErrorsMap(errors: List<String>): Map<String, List<String>> {
        val errorResponse: MutableMap<String, List<String>> = HashMap()
        errorResponse["errors"] = errors
        return errorResponse
    }

    @ExceptionHandler(CustomValidationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleCustomValidationException(ex: CustomValidationException): ResponseEntity<Map<String, String>> {
        val response = mapOf("error" to ex.message.orEmpty())
        return ResponseEntity(response, HttpStatus.BAD_REQUEST)
    }
}
