package com.xpartan.maqueta

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class ApiXpartanMaquetaApplication
fun main(args: Array<String>) {
	runApplication<ApiXpartanMaquetaApplication>(args = args)
}
