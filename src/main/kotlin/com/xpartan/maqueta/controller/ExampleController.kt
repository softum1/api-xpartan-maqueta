package com.xpartan.maqueta.controller

import com.xpartan.maqueta.model.ExampleDto
import com.xpartan.maqueta.service.IExampleService
import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.Valid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@RestController
@Validated
@RequestMapping("/api/v1/example")
class ExampleController {

    @Autowired
    private lateinit var exampleService: IExampleService

    companion object {
        val logger: Logger = LoggerFactory.getLogger(ExampleController::class.java)
    }

    @Operation(summary = "Endpoint de prueba que devuelve un 'Hello World'")
    @GetMapping("/hello-world")
    fun getHelloWorld() : ResponseEntity<*> {
        logger.debug("REST request to get Hello World")
        return ResponseEntity.ok(exampleService.getHelloWorld())
    }

    @Operation(summary = "Devuelve un Pageable de ExampleDto's")
    @GetMapping
    fun getAllExample(@RequestParam(defaultValue = "0") page: Int,
                      @RequestParam(defaultValue = "10") size: Int) : ResponseEntity<Page<*>> {
        logger.debug("REST request to get all example")
        val pageable: Pageable = PageRequest.of(page, size)
        return ResponseEntity.ok(exampleService.getAllExample(pageable))
    }

    @Operation(summary = "Devuelve un ExampleDto segun un ID dado")
    @GetMapping("/{id}")
    fun getExampleById(@PathVariable("id") id: Long) : ResponseEntity<*> {
        logger.debug("REST request to get example by id")
        return ResponseEntity.ok(exampleService.getExampleById(id))
    }

    @Operation(summary = "Guarda un ExampleDto")
    @PostMapping
    fun addExample(@RequestBody @Valid exampleDto: ExampleDto) : ResponseEntity<*>  {
        logger.debug("REST request to add example")
        return ResponseEntity.status(HttpStatus.CREATED)
            .body(exampleService.saveExample(exampleDto))
    }

    @Operation(summary = "Actualiza los datos de un ExampleDto si es que existe")
    @PutMapping("/{example_id}")
    fun updateExample(@RequestBody @Valid exampleDto: ExampleDto,
                      @PathVariable("example_id") exampleId: Long  ) : ResponseEntity<*> {
        logger.debug("REST request to update example")
        return ResponseEntity.ok(exampleService.updateExample(exampleDto, exampleId))
    }

    @Operation(summary = "Elimina un ExampleDto segun un ID dado")
    @DeleteMapping("/{id}")
    fun deleteExampleById(@PathVariable("id") id: Long) : ResponseEntity<*> {
        logger.debug("REST request to delete example by id")
        return ResponseEntity.ok(exampleService.deleteExample(id))
    }
}
