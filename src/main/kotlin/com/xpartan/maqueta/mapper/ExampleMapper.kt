package com.xpartan.maqueta.mapper

import com.xpartan.maqueta.domain.Example
import com.xpartan.maqueta.model.ExampleDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface ExampleMapper {

    @Mapping(target = "id", ignore = true)
    fun toEntity(dto: ExampleDto): Example

    fun toDto(entity: Example): ExampleDto
}
