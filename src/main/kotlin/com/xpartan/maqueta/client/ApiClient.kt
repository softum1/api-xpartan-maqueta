package com.xpartan.maqueta.client

import com.xpartan.maqueta.config.FeignConfiguration
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(name = "apiClient", configuration = [FeignConfiguration::class])
fun interface ApiClient {

    @GetMapping("/api/v1/categories/{id}")
    fun getCategoryById(@PathVariable id: Int) : Any
}
