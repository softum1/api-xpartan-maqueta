package com.xpartan.maqueta.model

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Size

data class ExampleDto(
    val id: Long,
    @get: NotBlank(message = "Title not be blank")
    @get: Size(min=3, max = 30, message = "Title length must be between 3 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\s]+$", message = "Title disallowed characters")
    val title: String
)
