package com.xpartan.maqueta.repository

import com.xpartan.maqueta.domain.Example
import org.springframework.data.jpa.repository.JpaRepository

interface ExampleRepository: JpaRepository<Example, Long>
