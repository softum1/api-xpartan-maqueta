 CREATE TABLE IF NOT EXISTS tbl_example (
	id int4 DEFAULT nextval('sequence_generator') NOT NULL,
	title varchar(255) NULL,
	CONSTRAINT tbl_example_pkey PRIMARY KEY (id)
);