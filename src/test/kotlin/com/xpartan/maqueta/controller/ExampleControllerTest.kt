package com.xpartan.maqueta.controller

import com.xpartan.maqueta.model.ExampleDto
import com.xpartan.maqueta.service.impl.ExampleServiceImpl
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import com.jayway.jsonpath.JsonPath as JsonParser
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ExampleControllerTest {

    private var wireMockServer: WireMockServer? = null

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var exampleController: ExampleController

    @Autowired
    private lateinit var exampleServiceImpl: ExampleServiceImpl

    @BeforeAll
    fun setup() {
        val validator = LocalValidatorFactoryBean()
        validator.afterPropertiesSet()
        mockMvc = MockMvcBuilders.standaloneSetup(exampleController)
            .setValidator(validator)
            .build()
        wireMockServer = WireMockServer(WireMockConfiguration().port(7071))
        wireMockServer!!.start()
        configureFor("localhost", 7071)
        RestAssured.baseURI = "http://localhost:7071"
    }

    @Test
    fun helloWorldTest(){
        stubFor(get("/hello-world")
            .willReturn(ok("Hello World!!!")))
        val res = RestAssured.given()
            .log().all()
            .get("/hello-world")
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/example/hello-world")
            .contentType(MediaType.TEXT_PLAIN_VALUE))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().string(res.body().asString()))
        reset()
    }

    @Test
    fun getAllExampleTest() {
        stubFor(get(urlEqualTo("/example"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("[{\"id\": 1,\"title\": \"Ejemplo 1\"}, {\"id\": 2,\"title\": \"Ejemplo 2\"}]"
                )))
        val response = RestAssured.given()
            .get("/example")
            .then()
            .statusCode(200)
            .extract().asString()
        val jsonPath = JsonParser.parse(response)

        val length: Int = jsonPath.read("$.length()", Int::class.java)

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/example")
            .param("page", "0")
            .param("size", "2")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfElements").value(length))
    }

    @Test
    fun getExampleById(){
        stubFor(get(urlEqualTo("/example/1"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .withBody("{\"id\": 1, \"title\": \"Ejemplo 1\"}")))

        val response = RestAssured.given()
            .get("/example/1")
            .then()
            .statusCode(200)
            .extract().asString();

        val jsonPath = JsonPath(response)

        mockMvc.perform(
            MockMvcRequestBuilders.get("/api/v1/example/1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(jsonPath.get<Int>("id").toString()))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(jsonPath.get<String>("title")))

    }

    @Test
    fun getExampleByIdFailed(){
        stubFor(get(urlEqualTo("/example/222"))
            .willReturn(aResponse()
                .withStatus(400)
                .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)))

        mockMvc.get("/api/v1/example/222")
            .andExpect {
                status { isBadRequest() }
            }
    }

    @Test
    fun updateExample(){
        val exampleId = 1L
        val exampleDto = ExampleDto(1, "Ejemplo 1")
        stubFor(
            put(urlEqualTo("/example/1"))
                .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                    .withBody("{\"id\": 1, \"title\": \"Ejemplo 1\"}")))

        val response = RestAssured.given()
            .body("{\"id\": 1, \"title\": \"Ejemplo 1\"}")
            .put("/example/1")
            .then()
            .statusCode(200)
            .extract().asString();

        val jsonPath = JsonPath(response)

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/example/{example_id}", exampleId)
            .contentType(MediaType.APPLICATION_JSON)
            .content(ObjectMapper()
                .writeValueAsString(exampleDto)))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(jsonPath.get<Int>("id").toString()))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(jsonPath.get<String>("title")))
    }

    @Test
    fun updateExampleFailed(){
        stubFor(
            put(urlEqualTo("/example/111"))
                .willReturn(aResponse()
                    .withStatus(400)
                    .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                    .withBody("{\"id\": 111, \"title\": \"Example Modified\"}")))

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/example/111")
            .contentType(MediaType.APPLICATION_JSON)
            .content(ObjectMapper()
                .writeValueAsString(ExampleDto(111, "Example Modified"))))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test
    fun addExample(){
        val exampleDto = ExampleDto(111, "Example Test")
        stubFor(
            post(urlEqualTo("/example"))
                .willReturn(aResponse()
                    .withStatus(201)
                    .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                    .withBody("{\"id\": 111, \"title\": \"Example Test\"}")))

        val response = RestAssured.given()
            .body("{\"id\": 111, \"title\": \"Example Test\"}")
            .post("/example")
            .then()
            .statusCode(201)
            .extract().asString();

        val jsonPath = JsonPath(response)

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/example")
            .contentType(MediaType.APPLICATION_JSON)
            .content(ObjectMapper()
                .writeValueAsString(exampleDto)))
            .andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(jsonPath.get<String>("title")))
    }


    @Test
    fun addExampleFailed(){
        stubFor(
            post(urlEqualTo("/example"))
                .willReturn(aResponse()
                    .withStatus(400)
                    .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                    .withBody("{\"id\": 111 }")))

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/example")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"id\": 111 }"))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
    }

    @Test
    fun deleteExample(){

        stubFor(delete(urlEqualTo("/example/1")).willReturn(noContent()))

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/example/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
    }


    @Test
    fun deleteExampleFailed(){

        stubFor(delete(urlEqualTo("/example/3")).willReturn(badRequest()))

        mockMvc.delete("/api/v1/example/222")
            .andExpect {
                status { isBadRequest() }
            }
    }
}
