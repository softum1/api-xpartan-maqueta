package com.xpartan.maqueta.exceptions

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException

@ExtendWith(MockitoExtension::class)
class GlobalErrorHandlerTest {

    @Mock
    private lateinit var ex: MethodArgumentNotValidException

    @Mock
    private lateinit var bindingResult: BindingResult

    @InjectMocks
    private lateinit var globalErrorHandler: GlobalErrorHandler

    @Test
    fun handleValidationErrorsTest() {
        `when`(ex.bindingResult).thenReturn(bindingResult)
        `when`(bindingResult.fieldErrors).thenReturn(listOf(FieldError("fieldName", "errorCode", "Default message")))

        val result: ResponseEntity<Map<String, List<String>>> = globalErrorHandler.handleValidationErrors(ex)

        assertThat(result.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
        assertThat(result.headers).isEqualTo(HttpHeaders.EMPTY)
        assertThat(result.body).isEqualTo(mapOf("errors" to listOf("Default message")))
    }

    @Test
    fun handleCustomValidationExceptionTest() {
        val customValidationException = CustomValidationException("Custom error message")

        val result: ResponseEntity<Map<String, String>> = globalErrorHandler
            .handleCustomValidationException(customValidationException)

        assertThat(result.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
        assertThat(result.headers).isEqualTo(HttpHeaders.EMPTY)
        assertThat(result.body).isEqualTo(mapOf("error" to "Custom error message"))
    }
}
