package com.xpartan.maqueta.service

import com.xpartan.maqueta.domain.Example
import com.xpartan.maqueta.exceptions.CustomValidationException
import com.xpartan.maqueta.mapper.ExampleMapper
import com.xpartan.maqueta.model.ExampleDto
import com.xpartan.maqueta.repository.ExampleRepository
import com.xpartan.maqueta.service.impl.ExampleServiceImpl
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.*
import java.util.*

@Profile("qa")
class ExampleServiceTest {

    @InjectMocks
    private lateinit var exampleServiceImpl: ExampleServiceImpl

    @Mock
    private lateinit var exampleMapper: ExampleMapper

    @Mock
    private lateinit var exampleRepository: ExampleRepository

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun getHelloWorldTest() {
        val result = exampleServiceImpl.getHelloWorld()
        assertEquals("Hello World!!!", result)
    }

    @Test
    fun getAllExampleTest() {
        val pageable = Pageable.unpaged()
        val example = Example(1, "Ej 1")
        val example2 = Example(2, "Ej 2")
        val exampleDto = ExampleDto(1, "Ej 1")
        val exampleDto2 = ExampleDto( 2, "Ej 2")
        val expectPage = PageImpl(listOf(example, example2))

        `when`(exampleRepository.findAll(pageable)).thenReturn(expectPage)
        `when`(exampleMapper.toDto(example)).thenReturn(exampleDto)
        `when`(exampleMapper.toDto(example2)).thenReturn(exampleDto2)

        val result = exampleServiceImpl.getAllExample(pageable)

        assertEquals(expectPage.totalElements, result.totalElements)
        assertEquals(2, result.content.size)
    }

    @Test
    fun getExampleByIdTest() {
        val id = 1L
        val exampleEntity = Example(id, "Example Title")
        val exampleDto = ExampleDto(id, "Example Title")

        `when`(exampleRepository.findById(id)).thenReturn(Optional.of(exampleEntity))
        `when`(exampleMapper.toDto(exampleEntity)).thenReturn(exampleDto)

        val result = exampleServiceImpl.getExampleById(id)
        assertEquals(exampleDto, result)
    }

    @Test
    fun getExampleByIdNotFoundTest() {
        val id = 1L
        `when`(exampleRepository.findById(id)).thenReturn(Optional.empty())
        assertThrows<CustomValidationException> {
            exampleServiceImpl.getExampleById(id)
        }
    }

    @Test
    fun saveExampleTest() {
        val id = 1L
        val exampleEntity = Example(id, "Example Title")
        val exampleDto = ExampleDto(id, "Example Title")
        `when`(exampleMapper.toEntity(exampleDto)).thenReturn(exampleEntity)
        `when`(exampleMapper.toDto(exampleEntity)).thenReturn(exampleDto)
        `when`(exampleRepository.save(exampleEntity)).thenReturn(exampleEntity)
        val result = exampleServiceImpl.saveExample(exampleDto)
        assertEquals(result, exampleDto)
    }

    @Test
    fun updateExampleTest() {
        val id = 1L
        val exampleEntity = Example(id, "Example Title")
        val exampleDto = ExampleDto(id, "Example Title")
        `when`(exampleRepository.findById(id)).thenReturn(Optional.of(exampleEntity))
        `when`(exampleRepository.save(exampleEntity)).thenReturn(exampleEntity)
        `when`(exampleMapper.toDto(exampleEntity)).thenReturn(exampleDto)
        val result = exampleServiceImpl.updateExample(exampleDto, id)
        assertEquals(exampleDto, result)
    }

    @Test
    fun updateExampleNotFoundTest() {
        val id = 1L
        val exampleDto = ExampleDto(id, "Example Title")
        `when`(exampleRepository.findById(id)).thenReturn(Optional.empty())
        assertThrows<CustomValidationException> {
            exampleServiceImpl.updateExample(exampleDto, id)
        }
    }

    @Test
    fun deleteExampleTest() {
        val id = 1L
        val exampleEntity = Example(id, "Example Title")
        `when`(exampleRepository.findById(id)).thenReturn(Optional.of(exampleEntity))
        exampleServiceImpl.deleteExample(id)
        Mockito.verify(exampleRepository, Mockito.times(1)).findById(id)
        Mockito.verify(exampleRepository, Mockito.times(1)).deleteById(id)
    }

    @Test
    fun deleteExampleNotFoundTest() {
        val id = 1L
        `when`(exampleRepository.findById(id)).thenReturn(Optional.empty())
        assertThrows<CustomValidationException> {
            exampleServiceImpl.deleteExample(id)
        }
    }
}

