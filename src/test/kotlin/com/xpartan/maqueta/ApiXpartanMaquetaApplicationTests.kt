package com.xpartan.maqueta

import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ApiXpartanMaquetaApplicationTests {

	@Mock
	private lateinit var api: ApiXpartanMaquetaApplication
	@Test
	fun contextLoads() {
	}

	@Test
	fun mainTest() {
		val args: Array<String> = arrayOf("test")
		main(args)
	}
}
